#include "common.hpp"
#include "utility.hpp"
#include "workspace/workspace_panel.hpp"

#include <wx/wx.h>
#include <wx/sysopt.h>

// Global TODO:
// * Serialization and deserialization must handle errors properly
// * Add table view
// * Do we even need a path information in the flash deck views?
// * What do we do about the groups and tags?

/// \brief The main window class
///
/// This class represents the main application window. Its other responsibility
/// is to capture user input from the menu and key chords and pass it to the
/// workspace widget for the further dispatch.
class MainFrame : public wxFrame {

    /// \brief The workspace panel
    ///
    workspace::WorkspacePanel m_workspace_panel;

    /// \brief Creates the menu bar widget
    ///
    wxMenuBar *m_create_menu_bar()
    {
        wxMenu *menu_file { new wxMenu };
        menu_file->Append(
            ID_LoadFlashDeck,
            "&Load flash deck from library...\tCtrl-L",
            "Load a library from file");
        menu_file->AppendSeparator();
        menu_file->Append(
            ID_LoadWorkspace,
            "Load &workspace...\tCtrl-W",
            "Load workspace from file");
        menu_file->Append(
            ID_SaveWorkspace,
            "&Save workspace...\tCtrl-S",
            "Save current workspace to file");
        menu_file->AppendSeparator();
        menu_file->Append(wxID_EXIT);

        wxMenu *menu_help = new wxMenu;
        menu_help->Append(wxID_ABOUT);

        wxMenuBar *result = new wxMenuBar;
        result->Append(menu_file, "&File");
        result->Append(menu_help, "&Help");

        return result;
    }

    /// \brief Set up the accelerators
    ///
    void m_setup_accelerators()
    {
        wxAcceleratorEntry entries[5];
        entries[0].Set(wxACCEL_CTRL, static_cast<int>('P'), ID_Chord_C_P);
        entries[1].Set(wxACCEL_ALT, static_cast<int>('P'), ID_Chord_M_P);
        entries[2].Set(wxACCEL_CTRL, static_cast<int>('N'), ID_Chord_C_N);
        entries[3].Set(wxACCEL_ALT, static_cast<int>('N'), ID_Chord_M_N);
        entries[4].Set(wxACCEL_CTRL, WXK_SPACE, ID_Chord_C_SPC);
        wxAcceleratorTable table(5, entries);
        SetAcceleratorTable(table);

        Bind(wxEVT_MENU, std::bind(&MainFrame::m_on_chord, this, 'C', 'P'), ID_Chord_C_P);
        Bind(wxEVT_MENU, std::bind(&MainFrame::m_on_chord, this, 'M', 'P'), ID_Chord_M_P);
        Bind(wxEVT_MENU, std::bind(&MainFrame::m_on_chord, this, 'C', 'N'), ID_Chord_C_N);
        Bind(wxEVT_MENU, std::bind(&MainFrame::m_on_chord, this, 'M', 'N'), ID_Chord_M_N);
        Bind(wxEVT_MENU, std::bind(&MainFrame::m_on_chord, this, 'C', WXK_SPACE), ID_Chord_C_SPC);
    }

    /// \brief Event for loading a library from a file
    ///
    void m_on_load_flash_deck(wxCommandEvent& event)
    {
        (void)event;
        m_workspace_panel.load_flash_deck_view();
    }

    /// \brief Loads a workspace from an xml file
    ///
    void m_on_load_workspace(wxCommandEvent& event)
    {
        (void)event;

        wxFileDialog dialog {
            this,
            "Open workspace file",
            "",
            "",
            "Workspace files|*.xml",
            wxFD_OPEN | wxFD_FILE_MUST_EXIST
        };

        if (dialog.ShowModal() == wxID_CANCEL) {
            return;
        }

        try {
            wxXmlDocument document { dialog.GetPath() };
            if (!document.IsOk()) {
                throw std::runtime_error { "Failed loading workspace document" };
            }
            m_workspace_panel.deserialize(document.GetRoot());
        } catch(const std::exception &ex) {
            utility::exception_notify("Loading workspace", ex);
        }
    }

    /// \brief Saves the current workspace into an xml file
    ///
    void m_on_save_workspace(wxCommandEvent& event)
    {
        (void)event;

        wxFileDialog dialog {
            this,
            "Save workspace file",
            "",
            "",
            "Workspace files|*.xml",
            wxFD_SAVE | wxFD_OVERWRITE_PROMPT
        };

        if (dialog.ShowModal() == wxID_CANCEL) {
            return;
        }

        wxXmlNode *node = m_workspace_panel.serialize();
        wxXmlDocument result;
        result.SetRoot(node);
        result.Save(dialog.GetPath());
    }

    /// \brief Exit event
    ///
    void m_on_exit(wxCommandEvent& event)
    {
        (void)event;
        Close(true);
    }

    /// \brief About event
    ///
    void m_on_about(wxCommandEvent& event)
    {
        (void)event;
        wxMessageBox(
            "This is a language learning support program",
            "About Lehren",
            wxOK | wxICON_INFORMATION);
    }

    /// \brief Generic chord callback
    ///
    void m_on_chord(int mod, int key)
    {
        m_workspace_panel.on_chord(mod, key);
    }

public:
    /// \brief Constructor
    ///
    /// \param title The window border title
    /// \param position The window position
    /// \param size The window size
    MainFrame(
        const wxString& title,
        const wxPoint& position,
        const wxSize& size) :
            wxFrame { nullptr, wxID_ANY, title, position, size },
            m_workspace_panel { this }
    {
        SetMenuBar(m_create_menu_bar());
        m_setup_accelerators();
    }

    wxDECLARE_EVENT_TABLE();
};

wxBEGIN_EVENT_TABLE(MainFrame, wxFrame)
    EVT_MENU(ID_LoadFlashDeck, MainFrame::m_on_load_flash_deck)
    EVT_MENU(ID_LoadWorkspace, MainFrame::m_on_load_workspace)
    EVT_MENU(ID_SaveWorkspace, MainFrame::m_on_save_workspace)
    EVT_MENU(wxID_EXIT, MainFrame::m_on_exit)
    EVT_MENU(wxID_ABOUT, MainFrame::m_on_about)
wxEND_EVENT_TABLE()

/// \brief The application class
///
class Application : public wxApp {
public:
    /// \biref The application initialization event handler
    ///
    bool OnInit() override
    {
        wxSystemOptions::SetOption("msw.remap", 2);
        MainFrame *frame {
            new MainFrame { "Lehren!", wxPoint(10, 10), wxSize(800, 600) }
        };
        frame->Show();
        return true;
    }
};

wxIMPLEMENT_APP(Application);
