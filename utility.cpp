#include "utility.hpp"

namespace utility {

    void exception_notify(const wxString& title, const std::exception& ex)
    {
        wxMessageBox(
            wxString("An Exception occured: ") << ex.what(),
            title,
            wxOK | wxICON_INFORMATION);
    }

}
