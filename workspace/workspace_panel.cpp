#include "workspace/workspace_panel.hpp"
#include "flash/flash_deck_load_dialog.hpp"

namespace workspace {

    void WorkspacePanel::m_on_view_update(flash::FlashDeckView *view)
    {
        const size_t page_count { m_notebook->GetPageCount() };
        for (size_t i = 0; i != page_count; ++i) {
            flash::FlashDeckView *view_i =
                dynamic_cast<flash::FlashDeckView*>(m_notebook->GetPage(i));
            if (view_i == view) {
                m_notebook->SetPageText(i, view->name);
            }
        }
    }

    wxSizer *WorkspacePanel::m_create_main_sizer()
    {
        m_notebook = new wxAuiNotebook(this, wxID_ANY);
        wxBoxSizer *main_sizer = new wxBoxSizer(wxVERTICAL);
        main_sizer->Add(m_notebook, 1, wxEXPAND);
        return main_sizer;
    }

    void WorkspacePanel::m_assert_workspace(wxXmlNode *node)
    {
        if (!node || node->GetName() != "workspace") {
            throw std::runtime_error {
                "Failed validating workspace node" };
        }
    }

    WorkspacePanel::WorkspacePanel(wxWindow *parent) :
        wxPanel(parent)
    {
        using namespace std::placeholders;

        m_view_update_channel.callbacks.push_back(
            std::bind(
                &WorkspacePanel::m_on_view_update, this, _1));

        SetSizer(m_create_main_sizer());
    }

    wxXmlNode *WorkspacePanel::serialize()
    {
        wxXmlNode *result =
            new wxXmlNode(wxXML_ELEMENT_NODE, "workspace");

        size_t page_count = m_notebook->GetPageCount();
        for (size_t i = 0; i != page_count; ++i) {
            flash::FlashDeckView *view =
                dynamic_cast<flash::FlashDeckView*>(m_notebook->GetPage(i));
            result->AddChild(view->serialize());
        }

        return result;
    }

    void WorkspacePanel::deserialize(wxXmlNode *source)
    {
        m_assert_workspace(source);

        m_notebook->DeleteAllPages();

        for (wxXmlNode *flash_deck_view_node = source->GetChildren();
             flash_deck_view_node;
             flash_deck_view_node = flash_deck_view_node->GetNext()) {

            if (flash_deck_view_node->GetType() != wxXML_ELEMENT_NODE) {
                continue;
            }

            flash::FlashDeckView *view =
                new flash::FlashDeckView(m_notebook, &m_view_update_channel);
            view->deserialize(flash_deck_view_node);
            m_notebook->AddPage(view, view->name);
        }
    }

    void WorkspacePanel::on_chord(int mod, int key)
    {
        ChordListener *as_listener =
            dynamic_cast<ChordListener*>(m_notebook->GetCurrentPage());
        if (as_listener) {
            as_listener->on_chord(mod, key);
        }
    }

    void WorkspacePanel::load_flash_deck_view()
    {
        flash::FlashDeckLoadDialog dialog(this);
        if (dialog.ShowModal() == wxID_CANCEL) {
            return;
        }
        try {
            flash::FlashDeckView *flash_deck_view =
                dialog.CreateView(m_notebook, &m_view_update_channel);
            m_notebook->AddPage(flash_deck_view, flash_deck_view->source_path);
        } catch(const std::exception& ex) {
            utility::exception_notify("Loading library", ex);
        }
    }

}
