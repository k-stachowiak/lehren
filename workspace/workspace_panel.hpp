#ifndef WORKSPACE_PANEL_HPP
#define WORKSPACE_PANEL_HPP

#include "utility.hpp"
#include "flash/flash_deck_view.hpp"

#include <wx/wx.h>
#include <wx/aui/auibook.h>

namespace workspace {

    /// \brief The panel containing all the learning views
    ///
    /// Object of this class manages learning aides presentation widgets. It is
    /// responsible for forwarding key chords to its children.
    ///
    /// Additionally it can be serialized to an XML tree and deserialized from
    /// one. To achieve that, it depends on the learning widgets to be
    /// serializable too.
    class WorkspacePanel : public wxPanel,
                           public utility::XmlSerializable,
                           public utility::ChordListener {

        /// Notebook presenting learning views
        ///
        wxAuiNotebook *m_notebook;

        /// Subject for view update notification
        ///
        utility::ViewUpdateChannel m_view_update_channel;

        /// \brief Callback for the view change
        ///
        /// This is supposed to be called whenever the underlying model has changed
        void m_on_view_update(flash::FlashDeckView *view);

        /// \brief Window layout delegated to a separated method
        ///
        wxSizer *m_create_main_sizer();

        /// \brief Assert the XML structural validity
        ///
        static void m_assert_workspace(wxXmlNode *node);

    public:
        /// \brief Constructor
        ///
        /// \param parent The parent window
        WorkspacePanel(wxWindow *parent);

        /// \brief Implementation of the XmlSerializable superclass
        ///
        wxXmlNode *serialize() override;

        /// \brief Implementation of the XmlSerializable superclass
        ///
        void deserialize(wxXmlNode *source) override;

        /// \brief Implementation of the ChordListener superclass
        ///
        void on_chord(int mod, int key) override;

        /// \brief Loads a new flash deck view
        ///
        void load_flash_deck_view();
    };

}

#endif
