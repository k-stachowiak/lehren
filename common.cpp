#include "common.hpp"

const int COMMON_BORDER_WIDTH = 4;

/// Standard entropy source
///
std::random_device rd;

/// Mersenne twister generator
///
std::mt19937 g(rd());
