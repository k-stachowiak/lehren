#include "xml_library.hpp"

#include <set>

void Library::m_assert_library(wxXmlNode *node)
{
    if (!node || node->GetName() != "library") {
        throw std::runtime_error {
            "Failed validating library node" };
    }
}

void Library::m_assert_dictionary(wxXmlNode *node)
{
    if (!node ||
        node->GetName() != "dictionary" ||
        !node->HasAttribute("name")) {
        throw std::runtime_error {
            "Failed validating dictionary node" };
    }
}

void Library::m_assert_binding(wxXmlNode *node)
{
    if (!node || node->GetName() != "bind") {
        throw std::runtime_error {
            "Failed validating binding node" };
    }
}

Library::Library(wxXmlDocument document) :
    m_doc { std::move(document) }
{
}

wxString Library::node_to_word(wxXmlNode *node)
{
    wxString word;
    if (node->HasAttribute("article")) {
        word += node->GetAttribute("article") + " ";
    }
    word += node->GetNodeContent();
    return word;
}

bool Library::good() const
{
    return m_doc.IsOk();
}

bool Library::empty() const
{
    if (!m_doc.IsOk()) {
        return true;
    }
    int counter = 0;
    for_each_binding_node(
        [&counter](wxXmlNode*, wxXmlNode *) { ++counter; });
    return !counter;
}

void Library::reset(wxXmlDocument doc)
{
    m_doc = doc;
}

void Library::for_each_dictionary_node(
        std::function<void(wxXmlNode*)> callback) const
{
    wxXmlNode *library_node = m_doc.GetRoot();
    m_assert_library(library_node);

    for (wxXmlNode *dictionary_node = library_node->GetChildren();
         dictionary_node;
         dictionary_node = dictionary_node->GetNext()) {

        if (dictionary_node->GetType() != wxXML_ELEMENT_NODE) {
            continue;
        }

        m_assert_dictionary(dictionary_node);
        callback(dictionary_node);
    }
}

void Library::for_each_binding_node(
        std::function<void(wxXmlNode *, wxXmlNode *)> callback) const
{
    for_each_dictionary_node(
        [&callback](wxXmlNode *dictionary_node)
        {
            for (wxXmlNode *binding_node = dictionary_node->GetChildren();
                 binding_node;
                 binding_node = binding_node->GetNext()) {

                if (binding_node->GetType() != wxXML_ELEMENT_NODE) {
                    continue;
                }

                m_assert_binding(binding_node);
                callback(dictionary_node, binding_node);
            }
        });
}

void Library::for_each_binding_node_in_dict(
        const wxString &dictionary_name,
        std::function<void(wxXmlNode *, wxXmlNode *)> callback) const
{
    for_each_dictionary_node(
        [&dictionary_name, &callback](wxXmlNode *dictionary_node)
        {
            if (dictionary_node->GetAttribute("name") != dictionary_name) {
                return;
            }
            for (wxXmlNode *binding_node = dictionary_node->GetChildren();
                 binding_node;
                 binding_node = binding_node->GetNext()) {

                if (binding_node->GetType() != wxXML_ELEMENT_NODE) {
                    continue;
                }

                m_assert_binding(binding_node);
                callback(dictionary_node, binding_node);
            }
        });
}

void Library::for_each_language_string(
        std::function<void (const wxString &)> callback) const
{
    std::set<wxString> languages;
    for_each_binding_node(
        [&languages](wxXmlNode *, wxXmlNode *binding_node)
        {
            for (wxXmlNode *language_node = binding_node->GetChildren();
                 language_node;
                 language_node = language_node->GetNext()) {

                if (language_node->GetType() != wxXML_ELEMENT_NODE) {
                    continue;
                }

                languages.insert(language_node->GetName());
            }
        });
    for (const wxString &language : languages) {
        callback(language);
    }
}

void Library::for_each_bound_pair(
        const wxString& from_key,
        const wxString& to_key,
        std::function<void(wxXmlNode *, wxXmlNode *, wxXmlNode *)> callback) const
{
    for_each_binding_node(
        [&from_key, &to_key, &callback](wxXmlNode *dictionary_node, wxXmlNode *binding_node) {

            std::vector<wxXmlNode*> froms;
            std::vector<wxXmlNode*> tos;

            for (wxXmlNode *word_node = binding_node->GetChildren();
                 word_node;
                 word_node = word_node->GetNext()) {

                if (word_node->GetType() != wxXML_ELEMENT_NODE) {
                    continue;
                }

                // The tests below aren't exclusive to handle a bizarre case
                // of mapping from language A to language A:

                if (word_node->GetName() == from_key) {
                    froms.push_back(word_node);
                }
                if (word_node->GetName() == to_key) {
                    tos.push_back(word_node);
                }
            }

            for (wxXmlNode* from : froms) {
                for (wxXmlNode* to : tos) {
                    callback(dictionary_node, from, to);
                }
            }

        });
}
