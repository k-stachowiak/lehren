#ifndef FLASH_DECK_VIEW_CONFIG_DIALOG_HPP
#define FLASH_DECK_VIEW_CONFIG_DIALOG_HPP

#include <wx/wx.h>
#include <wx/spinctrl.h>

namespace flash {

    /// \brief A flash deck view configuration dialog
    ///
    class FlashDeckViewConfigDialog : public wxDialog {

        wxTextCtrl *m_name_text_ctrl;      //< View name input
        wxSpinCtrl *m_page_size_spin_ctrl; //< Set cards per page value

        /// \brief Event for dialog resizing
        ///
        void m_on_size(wxSizeEvent& event);

    public:
        /// \brief Constructor
        ///
        /// Note that "old" meand "current at the time of the constructor's call".
        ///
        /// \param parent Parent window
        /// \param old_name The old (current) name
        /// \param old_page_size The old (current) page size parameter
        FlashDeckViewConfigDialog(
                wxWindow *parent,
                const wxString &old_name,
                int old_page_size);

        /// \brief Returns the new view name
        ///
        wxString GetName() const;

        /// \brief Returns the new cards per page value
        ///
        int GetPageSize() const;

        wxDECLARE_EVENT_TABLE();
    };

}

#endif
