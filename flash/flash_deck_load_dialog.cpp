#include "flash/flash_deck_load_dialog.hpp"
#include "common.hpp"

namespace flash {

    void FlashDeckLoadDialog::m_on_browse(wxCommandEvent& event)
    {
        (void)event;

        wxFileDialog dialog(
            this,
            "Open library file",
            "",
            "",
            "Library files|*.xml",
            wxFD_OPEN | wxFD_FILE_MUST_EXIST);

        if (dialog.ShowModal() == wxID_CANCEL) {
            return;
        }

        m_file_path_text_ctrl->Clear();
        m_source_language_choice->Clear();
        m_destination_language_choice->Clear();
        m_dictionary_check_list_box->Clear();

        try {
            m_library.reset(wxXmlDocument(dialog.GetPath()));
            m_file_path_text_ctrl->AppendText(dialog.GetPath());
            m_update_options_sizer();
            m_empty_info_panel->Hide();
            m_options_panel->Show();
            Fit();

        } catch(const std::exception& ex) {
            // TODO: Can't display message box during ShowModal, think of something else here
            std::cerr << "Exception whild loading library" << ex.what() << std::endl;
            m_library.reset(wxXmlDocument {});
            m_empty_info_panel->Show();
            m_options_panel->Hide();
            Fit();
        }

    }

    void FlashDeckLoadDialog::m_on_size(wxSizeEvent& event)
    {
        (void)event;
        Layout();
    }

    void FlashDeckLoadDialog::m_initialize_file_load_sizer()
    {
        /// The layout is the following:
        /// ----------------------------------------------
        /// | prompt string | path input | browse button |
        /// ----------------------------------------------

        m_file_load_sizer = new wxBoxSizer(wxHORIZONTAL);

        m_file_path_text_ctrl = new wxTextCtrl(this, wxID_ANY);
        wxButton *browse_button = new wxButton(this, ID_Browse, "...");

        m_file_load_sizer->Add(new wxStaticText(this, wxID_ANY, "File path:"),
                               0,
                               wxALIGN_CENTER_VERTICAL);
        m_file_load_sizer->Add(m_file_path_text_ctrl, 1, wxEXPAND);
        m_file_load_sizer->Add(browse_button, 0, 0);
    }

    void FlashDeckLoadDialog::m_initialize_options_sizer()
    {
        /// The layout is the flollowing:
        /// -------------------------------------
        /// | source language        | switches |
        /// | ---------------------- | ...      |
        /// | destination language   |          |
        /// | ---------------------- |          |
        /// | dictionaries selection |          |
        /// -------------------------------------

        wxBoxSizer *temp_sizer;

        m_options_panel = new wxPanel(this, wxID_ANY);

        wxBoxSizer *options_sizer = new wxBoxSizer(wxHORIZONTAL);

        // 1. Initialization of the filter (left) area:
        wxBoxSizer *filter_sizer = new wxBoxSizer(wxVERTICAL);

        m_source_language_choice = new wxChoice(m_options_panel, wxID_ANY);
        temp_sizer = new wxBoxSizer(wxHORIZONTAL);
        temp_sizer->Add(new wxStaticText(m_options_panel, wxID_ANY, "Source language:"),
                        0,
                        wxALIGN_CENTER_VERTICAL);
        temp_sizer->Add(m_source_language_choice, 1, wxEXPAND);
        filter_sizer->Add(temp_sizer, 0, wxEXPAND);

        m_destination_language_choice = new wxChoice(m_options_panel, wxID_ANY);
        temp_sizer = new wxBoxSizer(wxHORIZONTAL);
        temp_sizer->Add(new wxStaticText(m_options_panel, wxID_ANY, "Destination language:"),
                        0,
                        wxALIGN_CENTER_VERTICAL);
        temp_sizer->Add(m_destination_language_choice, 1, wxEXPAND);
        filter_sizer->Add(temp_sizer, 0, wxEXPAND);

        m_dictionary_check_list_box = new wxCheckListBox(m_options_panel, wxID_ANY);
        filter_sizer->Add(new wxStaticText(m_options_panel, wxID_ANY, "Dictionary filter:"));
        filter_sizer->Add(m_dictionary_check_list_box, 1, wxEXPAND);

        // 2. Initialization of the switches (right) area:
        wxBoxSizer *switches_sizer = new wxBoxSizer(wxVERTICAL);

        m_source_aggregation_checkbox =
            new wxCheckBox(m_options_panel, wxID_ANY, "aggregate sources");
        m_source_aggregation_checkbox->SetValue(true);
        m_destination_aggregation_checkbox =
            new wxCheckBox(m_options_panel, wxID_ANY, "aggregate destinations");
        m_destination_aggregation_checkbox->SetValue(true);
        switches_sizer->Add(new wxStaticText(m_options_panel,
                                             wxID_ANY,
                                             "Library loading options:"));
        switches_sizer->Add(m_source_aggregation_checkbox);
        switches_sizer->Add(m_destination_aggregation_checkbox);

        // Populate the main sizer:
        options_sizer->Add(filter_sizer, 1, wxEXPAND | wxALL, COMMON_BORDER_WIDTH);
        options_sizer->Add(switches_sizer, 1, wxEXPAND | wxALL, COMMON_BORDER_WIDTH);
        m_options_panel->SetSizer(options_sizer);
    }

    void FlashDeckLoadDialog::m_update_options_sizer()
    {
        // 1. Fill in the language choice controls:
        int languages_count = 0;
        m_library.for_each_language_string(
            [this, &languages_count](const wxString &language)
            {
                m_source_language_choice->Append(language);
                m_destination_language_choice->Append(language);
                ++languages_count;
            });

        if (languages_count > 0) {
            m_source_language_choice->SetSelection(0);
            m_destination_language_choice->SetSelection(0);
        }

        if (languages_count > 1) {
            m_destination_language_choice->SetSelection(1);
        }

        // 2. Fill in the dictionary filter list:
        int dictionaries_count = 0;
        m_library.for_each_dictionary_node(
            [this, &dictionaries_count](wxXmlNode *dictionary_node)
            {
                m_dictionary_check_list_box->Append(
                    dictionary_node->GetAttribute("name"));
                ++dictionaries_count;
            });

        for (int i = 0; i != dictionaries_count; ++i) {
            m_dictionary_check_list_box->Check(i);
        }
    }

    FlashDeckLoadDialog::FlashDeckLoadDialog(wxWindow *parent) :
        wxDialog(
            parent,
            wxID_ANY,
            "Load library...",
            wxDefaultPosition,
            wxDefaultSize,
            wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER)
    {
        // The main box diviting the dialog like this:
        // -------------------------
        // | 1. File load section  |
        // -------------------------
        // | 2. The file dependent |
        // |    content            |
        // -------------------------
        // | 3. The buttons        |
        // -------------------------
        wxBoxSizer *main_sizer = new wxBoxSizer(wxVERTICAL);

        // 1. The file loading section
        m_initialize_file_load_sizer();

        // 2. The file dependent content comes in two variants:
        // 2.a) A panel with prompt to load a file,
        // 2.b) A panel allowing tailoring and filtering input library
        //
        // At first the 2.a) is shown in this area, but whenever a new file is
        // loaded a refreshed 2.b) is shown here.
        m_switching_sizer = new wxBoxSizer(wxHORIZONTAL);

        m_empty_info_panel = new cmp::CenteredLabelPanel(
            this, "Define the library file to see loading options");
        m_initialize_options_sizer();

        // Populate the switching sizer:
        m_switching_sizer->Add(m_empty_info_panel, 1, wxEXPAND | wxALL);
        m_switching_sizer->Add(m_options_panel, 1, wxEXPAND | wxALL);

        m_options_panel->Hide();

        // 3. The dialog buttons sizer:
        wxSizer *buttons_sizer = CreateButtonSizer(wxOK | wxCANCEL);

        // Populate and set the main sizer:
        main_sizer->Add(m_file_load_sizer, 0, wxEXPAND | wxALL, COMMON_BORDER_WIDTH);
        main_sizer->Add(m_switching_sizer, 1, wxEXPAND | wxALL, COMMON_BORDER_WIDTH);
        main_sizer->Add(buttons_sizer, 0, wxEXPAND | wxALL, COMMON_BORDER_WIDTH);
        SetSizerAndFit(main_sizer);
        Layout();
    }

    FlashDeckView *FlashDeckLoadDialog::CreateView(
            wxWindow *parent,
            utility::ViewUpdateChannel *view_update_channel) const
    {
        // Precompute the list of the selected dictionaries
        std::vector<wxString> dictionaries;
        wxArrayInt checked_items;
        unsigned int checked_items_count =
            m_dictionary_check_list_box->GetCheckedItems(checked_items);
        while (checked_items_count) {
            dictionaries.push_back(
                m_dictionary_check_list_box->GetString(checked_items[checked_items_count - 1]));
            --checked_items_count;
        }

        // Construct the result object
        return new FlashDeckView(
                parent,
                view_update_channel,
                m_file_path_text_ctrl->GetValue(),
                m_library,
                dictionaries,
                m_source_language_choice->GetString(
                    m_source_language_choice->GetSelection()),
                m_destination_language_choice->GetString(
                    m_destination_language_choice->GetSelection()),
                m_source_aggregation_checkbox->IsChecked(),
                m_destination_aggregation_checkbox->IsChecked());
    }

    wxBEGIN_EVENT_TABLE(FlashDeckLoadDialog, wxDialog)
        EVT_BUTTON(ID_Browse, FlashDeckLoadDialog::m_on_browse)
        EVT_SIZE(FlashDeckLoadDialog::m_on_size)
    wxEND_EVENT_TABLE()
}
