#ifndef FLASH_CARD_PANEL_HPP
#define FLASH_CARD_PANEL_HPP

#include "component/revealable_string_panel.hpp"

#include <wx/wx.h>

namespace flash {

    /// \brief A panel representing the flashcard
    ///
    /// The panel consists of two areas one for the "question" part and one for the
    /// "answer" part. It supports two states:
    /// - The question state, in which only the question area is revealed,
    /// - The answer state, in which both areas are revealed.
    class FlashCardPanel : public wxPanel {

        cmp::RevealableStringPanel *m_string_panel1; //< The question area control
        cmp::RevealableStringPanel *m_string_panel2; //< The answer area control

    public:
        /// \brief A constructor
        ///
        /// \param parent A pointer to the parent object needed by the base class
        FlashCardPanel(wxWindow *parent);

        /// \brief Resets the control to a new question/answer pair in a question state
        ///
        /// \param string1 The question string
        /// \param string2 The answer string
        void reset(const wxString& string1, const wxString& string2);

        /// \brief Transition to the answer state
        ///
        void reveal();
    };

}

#endif
