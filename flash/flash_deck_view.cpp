#include "flash/flash_deck_view.hpp"
#include "flash/flash_deck_view_config_dialog.hpp"
#include "common.hpp"

namespace flash {

    bool FlashDeckView::m_has_dictionary(const wxString &dictionary_name) const
    {
        auto it = std::find(dictionaries.begin(), dictionaries.end(), dictionary_name);
        return it != dictionaries.end();
    }

    int FlashDeckView::m_first_index_in_page(int page)
    {
        return page * m_page_size;
    }

    int FlashDeckView::m_last_index_in_page(int page)
    {
        // We take the first index + size, accounting for the fact that the last
        // page has less cards.
        return std::min(
            m_first_index_in_page(page) + m_page_size,
            (int)m_all_pairs.size());
    }

    int FlashDeckView::m_current_page_size()
    {
        int page = current_page();
        return
            m_last_index_in_page(page) -
            m_first_index_in_page(page);
    }

    int FlashDeckView::m_current_index_in_page()
    {
        return m_current_index % m_page_size;
    }

    void FlashDeckView::m_on_swap_languages(wxCommandEvent &event)
    {
        (void)event;
        std::swap(source_language, destination_language);
        std::swap(aggregate_sources, aggregate_destinations);
        m_rebuild_pairs_index();
        m_update_controls();
    }

    void FlashDeckView::m_on_shuffle_bindings(wxCommandEvent &event)
    {
        (void)event;
        std::shuffle(m_all_pairs.begin(), m_all_pairs.end(), g);
        m_update_controls();
    }

    void FlashDeckView::m_on_configure(wxCommandEvent &event)
    {
        (void)event;
        FlashDeckViewConfigDialog dialog(
                this,
                name,
                m_page_size);
        if (dialog.ShowModal() == wxID_CANCEL) {
            return;
        }
        name = dialog.GetName();
        set_page_size(dialog.GetPageSize());
        m_view_update_channel->broadcast(this);
    }

    void FlashDeckView::m_assert_dictionary(wxXmlNode *node)
    {
        if (!node || node->GetName() != "dictionary") {
            throw std::runtime_error {
                "Failed validating dictionary node" };
        }
        int children_count = 0;
        for (wxXmlNode *child = node->GetChildren(); child; child = child->GetNext()) {
            if (children_count == 0 && child->GetType() != wxXML_TEXT_NODE) {
                throw std::runtime_error {
                    "Incorrect child type in dictionary" };
            }
            ++children_count;
        }
        if (children_count != 1) {
            throw std::runtime_error {
                "Incorrect number of flash deck view children" };
        }
    }

    void FlashDeckView::m_assert_flash_deck_view(wxXmlNode *node)
    {
        if (!node) {
            throw std::runtime_error {
                "Failed validating flash deck view node" };
        }

        if (node->GetName() != "flash-deck-view") {
            throw std::runtime_error {
                "Failed validating flash deck view node" };
        }

        if (!node->HasAttribute("source-path")) {
            throw std::runtime_error {
                "Flash deck view node is missing source-path attribute" };
        }

        if (!node->HasAttribute("source-language")) {
            throw std::runtime_error {
                "Flash deck view node is missing source-language attribute" };
        }

        if (!node->HasAttribute("destination-language")) {
            throw std::runtime_error {
                "Flash deck view node is missing destination-language attribute" };
        }

        if (!node->HasAttribute("aggregate-sources")) {
            throw std::runtime_error {
                "Flash deck view node is missing aggregate-sources attribute" };
        }

        if (!node->HasAttribute("aggregate-destinations")) {
            throw std::runtime_error {
                "Flash deck view node is missing aggregate-destinations attribute" };
        }

        int children_count = 0;
        for (wxXmlNode *child = node->GetChildren(); child; child = child->GetNext()) {
            if (children_count == 0 && child->GetName() != "dictionaries") {
                throw std::runtime_error {
                    "Incorrect name of the first flash deck view child: " +
                        child->GetName() };
            }
            ++children_count;
        }
        if (children_count > 1) {
            throw std::runtime_error {
                "Incorrect number of flash deck view children" };
        }
    }

    void FlashDeckView::m_on_page_previous(wxCommandEvent &event)
    {
        (void)event;
        decrement_page();
    }

    void FlashDeckView::m_on_card_previous(wxCommandEvent &event)
    {
        (void)event;
        decrement_card();
    }

    void FlashDeckView::m_on_card_next(wxCommandEvent &event)
    {
        (void)event;
        increment_card();
    }

    void FlashDeckView::m_on_page_next(wxCommandEvent &event)
    {
        (void)event;
        increment_page();
    }

    void FlashDeckView::m_update_controls()
    {
      if (empty()) {
            Disable();
            m_flash_card_panel->Disable();
        } else {
            Enable();
            m_flash_card_panel->Enable();

            FlashDeckBinding binding = m_all_pairs[m_current_index];
            m_flash_card_panel->reset(binding.from, binding.to);
            m_pages_counter->SetLabel(
                wxString("Page : ") << (current_page() + 1) << "/" << pages_count());
            m_words_counter->SetLabel(
                wxString("Word : ") << (m_current_index_in_page() + 1)
                << "/" << m_current_page_size());
            m_dictionary_text_ctrl->SetLabel("Dictionary : " + binding.source_dictionary);

            GetSizer()->Layout();
        }
    }

    void FlashDeckView::m_rebuild_pairs_index()
    {
        // 1. Clear view data:
        m_all_pairs.clear();

        // 2. Check current library for validity:
        if (empty()) {
            return;
        }

        // 3. Rebuild index
        m_for_each_binding_node(
            [this](wxXmlNode *dictionary_node, wxXmlNode *binding_node) {

                // 3.1. Read all the from and to words from the binding:
                std::vector<wxString> froms;
                std::vector<wxString> tos;
                for (wxXmlNode *word_node = binding_node->GetChildren();
                     word_node;
                     word_node = word_node->GetNext()) {

                    if (word_node->GetType() != wxXML_ELEMENT_NODE) {
                        continue;
                    }

                    if (word_node->GetName() == source_language) {
                        froms.push_back(Library::node_to_word(word_node));
                    }
                    if (word_node->GetName() == destination_language) {
                        tos.push_back(Library::node_to_word(word_node));
                    }
                }

                // 3.2. Insert binding's data into the common map:
                if (aggregate_sources && aggregate_destinations) {
                    // 3.2.a) Aggregate both ends:
                    m_all_pairs.push_back(
                        FlashDeckBinding {
                            dictionary_node->GetAttribute("name"),
                            utility::join_strings(froms.begin(), froms.end(), " | "),
                            utility::join_strings(tos.begin(), tos.end(), " | ") });
                } else if (aggregate_sources && !aggregate_destinations) {
                    // 3.2.b) Only sources aggregated:
                    wxString aggregated_froms = utility::join_strings(froms.begin(),
                                                                      froms.end(),
                                                                      " | ");
                    for (const wxString &to : tos) {
                        m_all_pairs.push_back(
                            FlashDeckBinding {
                                dictionary_node->GetAttribute("name"),
                                aggregated_froms,
                                to });
                    }
                } else if (!aggregate_sources && aggregate_destinations) {
                    // 3.2.c) Only destinations aggregated:
                    wxString aggregated_tos = utility::join_strings(tos.begin(), tos.end(), " | ");
                    for (const wxString &from : froms) {
                        m_all_pairs.push_back(
                            FlashDeckBinding {
                                dictionary_node->GetAttribute("name"),
                                from,
                                aggregated_tos });
                    }
                } else if (!aggregate_sources && !aggregate_destinations) {
                    // 3.2.d) None aggregated:
                    for (const wxString &from : froms) {
                        for (const wxString &to : tos) {
                            m_all_pairs.push_back(
                                FlashDeckBinding {
                                    dictionary_node->GetAttribute("name"),
                                    from,
                                    to });
                        }
                    }
                }
            });

        // 5. Update presentation
        m_update_controls();
    }

    void FlashDeckView::m_initialize_controls()
    {
        m_dictionary_text_ctrl = new wxStaticText(this, wxID_ANY, wxEmptyString);

        wxBitmap swap_bitmap("icons/swap32.png", wxBITMAP_TYPE_PNG);
        wxBitmap shuffle_bitmap("icons/shuffle32.png", wxBITMAP_TYPE_PNG);
        wxBitmap configure_bitmap("icons/configure32.png", wxBITMAP_TYPE_PNG);

        m_tool_bar = new wxToolBar(
            this,
            wxID_ANY,
            wxDefaultPosition,
            wxDefaultSize,
            wxTB_FLAT | wxTB_HORIZONTAL);

        m_tool_bar->AddTool(ID_SwapLanguages, "Swap languages", swap_bitmap);
        m_tool_bar->AddTool(ID_ShuffleBindings, "Shuffle bindings", shuffle_bitmap);
        m_tool_bar->AddTool(ID_Configure, "Configure view", configure_bitmap);
        m_tool_bar->Realize();

        m_flash_card_panel = new FlashCardPanel(this);

        wxBitmap page_prev_bitmap("icons/rew16.png", wxBITMAP_TYPE_PNG);
        wxBitmap card_prev_bitmap("icons/previous16.png", wxBITMAP_TYPE_PNG);
        wxBitmap card_next_bitmap("icons/next16.png", wxBITMAP_TYPE_PNG);
        wxBitmap page_next_bitmap("icons/ff16.png", wxBITMAP_TYPE_PNG);

        m_pages_counter = new wxStaticText(this, wxID_ANY, " ");
        m_words_counter = new wxStaticText(this, wxID_ANY, " ");

        wxBoxSizer *inner_control_sizer = new wxBoxSizer(wxHORIZONTAL);
        inner_control_sizer->Add(m_pages_counter, 0, wxALIGN_CENTER_VERTICAL);
        inner_control_sizer->AddStretchSpacer(1);
        inner_control_sizer->Add(new wxBitmapButton(this, ID_PagePrevious, page_prev_bitmap), 0);
        inner_control_sizer->Add(new wxBitmapButton(this, ID_CardPrevious, card_prev_bitmap), 0);
        inner_control_sizer->Add(new wxBitmapButton(this, ID_CardNext, card_next_bitmap), 0);
        inner_control_sizer->Add(new wxBitmapButton(this, ID_PageNext, page_next_bitmap), 0);
        inner_control_sizer->AddStretchSpacer(1);
        inner_control_sizer->Add(m_words_counter, 0, wxALIGN_CENTER_VERTICAL);

        wxBoxSizer *control_sizer = new wxBoxSizer(wxVERTICAL);
        control_sizer->AddStretchSpacer();
        control_sizer->Add(inner_control_sizer, 0, wxEXPAND | wxALL, 4);
        control_sizer->AddStretchSpacer();

        wxBoxSizer *main_sizer = new wxBoxSizer(wxVERTICAL);
        main_sizer->Add(m_tool_bar, 0, wxEXPAND);
        main_sizer->Add(m_dictionary_text_ctrl, 0, wxEXPAND | wxALL, COMMON_BORDER_WIDTH);
        main_sizer->Add(m_flash_card_panel, 1, wxEXPAND);
        main_sizer->Add(control_sizer, 0, wxEXPAND);
        SetSizer(main_sizer);
    }

    void FlashDeckView::m_initialize_configuration()
    {
        m_page_size = 7;
        m_current_index = 0;
    }

    FlashDeckView::FlashDeckView(
            wxWindow *parent,
            utility::ViewUpdateChannel *view_update_channel) :
        wxPanel(parent),
        m_view_update_channel(view_update_channel)
    {
        m_initialize_controls();
        m_initialize_configuration();
    }

    FlashDeckView::FlashDeckView(
            wxWindow *parent,
            utility::ViewUpdateChannel *view_update_channel,
            const wxString &source_path,
            const Library &library,
            const std::vector<wxString> &dictionaries,
            const wxString &source_language,
            const wxString &destination_language,
            bool aggregate_sources,
            bool aggregate_destinations) :
        wxPanel(parent),
        m_view_update_channel(view_update_channel),
        source_path(source_path),
        name(source_path),
        library(library),
        dictionaries(dictionaries),
        source_language(source_language),
        destination_language(destination_language),
        aggregate_sources(aggregate_sources),
        aggregate_destinations(aggregate_destinations)
    {
        m_initialize_controls();
        m_initialize_configuration();
        m_rebuild_pairs_index();
    }

    wxXmlNode *FlashDeckView::serialize()
    {
        wxXmlNode *result =
            new wxXmlNode(wxXML_ELEMENT_NODE, "flash-deck-view");

        result->AddAttribute("name", name);
        result->AddAttribute("source-path", source_path);
        result->AddAttribute("source-language", source_language);
        result->AddAttribute("destination-language", destination_language);
        result->AddAttribute("page-size", wxString::Format(wxT("%i"), m_page_size));

        result->AddAttribute(
            "aggregate-sources",
            aggregate_sources ? "true" : "false");

        result->AddAttribute(
            "aggregate-destinations",
            aggregate_destinations ? "true" : "false");

        wxXmlNode *dictionaries_node =
            new wxXmlNode(wxXML_ELEMENT_NODE, "dictionaries");

        for (const wxString &dictionary_name : dictionaries) {
            wxXmlNode *dictionary_node =
                new wxXmlNode(wxXML_ELEMENT_NODE, "dictionary");
            dictionary_node->AddChild(
                new wxXmlNode(wxXML_TEXT_NODE, "", dictionary_name));
            dictionaries_node->AddChild(dictionary_node);
        }
        result->AddChild(dictionaries_node);

        return result;
    }

    void FlashDeckView::deserialize(wxXmlNode *source)
    {
        m_assert_flash_deck_view(source);

        source_path = source->GetAttribute("source-path");

        if (source->HasAttribute("name")) {
            name = source->GetAttribute("name");
        } else {
            name = source_path;
        }

        if (source->HasAttribute("page-size")) {
            long value;
            if (!source->GetAttribute("page-size").ToLong(&value)) {
                throw std::runtime_error { "Failed reading page size" };
            }
            m_page_size = static_cast<int>(value);
        } else {
            name = source_path;
        }

        try {
            wxXmlDocument new_library(source_path);
            library = new_library;
        } catch(const std::exception& ex) {
            std::cerr <<
                "Failed loading file " + source_path + " into workspace" <<
                std::endl;
            return;
        }

        source_language =
            source->GetAttribute("source-language");
        destination_language =
            source->GetAttribute("destination-language");
        wxString aggregate_sources_string =
            source->GetAttribute("aggregate-sources");
        if (aggregate_sources_string == "true") {
            aggregate_sources = true;
        } else if (aggregate_sources_string == "false") {
            aggregate_sources = false;
        } else {
            throw std::runtime_error {
                "Unexpected value instead of bool: " + aggregate_sources_string };
        }
        wxString aggregate_destinations_string =
            source->GetAttribute("aggregate-destinations");
        if (aggregate_destinations_string == "true") {
            aggregate_destinations = true;
        } else if (aggregate_destinations_string == "false") {
            aggregate_destinations = false;
        } else {
            throw std::runtime_error {
                "Unexpected value instead of bool: " + aggregate_destinations_string };
        }

        wxXmlNode *dictionaries_node = source->GetChildren();
        dictionaries.clear();
        for (wxXmlNode *dictionary_node = dictionaries_node->GetChildren();
             dictionary_node;
             dictionary_node = dictionary_node->GetNext()) {

            if (dictionary_node->GetType() != wxXML_ELEMENT_NODE) {
                continue;
            }

            m_assert_dictionary(dictionary_node);

            dictionaries.push_back(dictionary_node->GetNodeContent());
        }

        m_rebuild_pairs_index();
    }

    void FlashDeckView::on_chord(int mod, int key)
    {
        switch (mod) {
        case 'M':
            switch (key) {
            case 'N':
                increment_page();
                break;
            case 'P':
                decrement_page();
                break;
            }
            break;
        case 'C':
            switch (key) {
            case 'N':
                increment_card();
                break;
            case 'P':
                decrement_card();
                break;
            case WXK_SPACE:
                m_flash_card_panel->reveal();
                break;
            }
            break;
        }
    }

    bool FlashDeckView::empty() const
    {
        return !library.good() || library.empty();
    }

    int FlashDeckView::pages_count()
    {
        if (empty()) {
            return 0;
        }
        return
            m_all_pairs.size() / m_page_size +    // The number of the full pages
            !!(m_all_pairs.size() % m_page_size); // If there is a reminder, add one
    }

    int FlashDeckView::current_page()
    {
        if (empty()) {
            return 0;
        }
        return m_current_index / m_page_size;
    }

    void FlashDeckView::set_page_size(int page_size)
    {
        m_page_size = page_size;
        m_update_controls();
    }

    void FlashDeckView::decrement_page()
    {
        // 1. Update state
        int page = current_page();
        if (page == 0) {
            page = pages_count();
        }
        --page;
        m_current_index = m_first_index_in_page(page);

        // 2. Update presentation
        m_update_controls();
    }

    void FlashDeckView::decrement_card()
    {
        // 1. Update state
        int page = current_page();
        int index_in_page = m_current_index_in_page();
        if (index_in_page == 0) {
            m_current_index = m_last_index_in_page(page);
        }
        --m_current_index;

        // 2. Update presentation
        m_update_controls();
    }

    void FlashDeckView::increment_card()
    {
        // 1. Update state
        int page = current_page();
        ++m_current_index;
        int index_in_page = m_current_index_in_page();
        if (index_in_page == 0 || m_current_index == (int)m_all_pairs.size()) {
            // The only way of getting here is exceeding a page or the entire collection
            m_current_index = m_first_index_in_page(page);
        }

        // 2. Update presentation
        m_update_controls();
    }

    void FlashDeckView::increment_page()
    {
        // 1. Update state
        int page = current_page();
        ++page;
        if (page == pages_count()) {
            page = 0;
        }
        m_current_index = m_first_index_in_page(page);

        // 2. Update presentation
        m_update_controls();
    }

    wxBEGIN_EVENT_TABLE(FlashDeckView, wxPanel)
        EVT_TOOL(ID_SwapLanguages, FlashDeckView::m_on_swap_languages)
        EVT_TOOL(ID_ShuffleBindings, FlashDeckView::m_on_shuffle_bindings)
        EVT_TOOL(ID_Configure, FlashDeckView::m_on_configure)
        EVT_BUTTON(ID_PagePrevious, FlashDeckView::m_on_page_previous)
        EVT_BUTTON(ID_CardPrevious, FlashDeckView::m_on_card_previous)
        EVT_BUTTON(ID_CardNext, FlashDeckView::m_on_card_next)
        EVT_BUTTON(ID_PageNext, FlashDeckView::m_on_page_next)
    wxEND_EVENT_TABLE()

}
