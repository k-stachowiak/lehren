#ifndef FLASH_DECK_LOAD_DIALOG_HPP
#define FLASH_DECK_LOAD_DIALOG_HPP

#include "xml_library.hpp"
#include "flash/flash_deck_view.hpp"
#include "component/centered_label_panel.hpp"

#include <wx/wx.h>

namespace flash {

    /// \brief A customized library loading dialog
    ///
    class FlashDeckLoadDialog : public wxDialog {

        /// The underlying data source
        ///
        Library m_library;

        /// Presents the file to be loaded
        ///
        wxTextCtrl *m_file_path_text_ctrl;

        /// Displays prompt for loading a library file
        ///
        wxBoxSizer *m_file_load_sizer;

        /// Manages switching between the main view content based on whether there
        /// is a valid library pointed by this loading dialog or not.
        wxBoxSizer *m_switching_sizer;

        /// Panel displayed whenever there is no valid library selected
        ///
        cmp::CenteredLabelPanel *m_empty_info_panel;

        /// Panel for setting options for loading of the selected library
        ///
        wxPanel *m_options_panel;

        /// Selection of the source language
        ///
        wxChoice *m_source_language_choice;

        /// Selection of the destination language
        ///
        wxChoice *m_destination_language_choice;

        /// Selection of the dictionaries in the library
        ///
        wxCheckListBox *m_dictionary_check_list_box;

        /// Enable aggregating source words
        ///
        wxCheckBox *m_source_aggregation_checkbox;

        /// Enable aggregating destination words
        ///
        wxCheckBox *m_destination_aggregation_checkbox;

        /// \brief Event for selection of a library file
        ///
        void m_on_browse(wxCommandEvent& event);

        /// \brief Event for dialog resizing
        ///
        void m_on_size(wxSizeEvent& event);

        /// \brief Create and lay out the file load prompt
        ///
        void m_initialize_file_load_sizer();

        /// \brief Create and lay out the options controls
        ///
        void m_initialize_options_sizer();

        /// \brief Update the options controls based on the given library
        ///
        void m_update_options_sizer();

    public:
        /// \brief Constructor
        ///
        FlashDeckLoadDialog(wxWindow *parent);

        /// \brief Create view based on the dialog state
        ///
        FlashDeckView *CreateView(
                wxWindow *parent,
                utility::ViewUpdateChannel *view_update_channel) const;

        wxDECLARE_EVENT_TABLE();
    };

}

#endif
