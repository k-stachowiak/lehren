#include "flash/flash_card_panel.hpp"

namespace flash {

    FlashCardPanel::FlashCardPanel(wxWindow *parent) :
            wxPanel(parent)
    {
        wxBoxSizer *sizer = new wxBoxSizer(wxVERTICAL);
        m_string_panel1 = new cmp::RevealableStringPanel(this);
        m_string_panel2 = new cmp::RevealableStringPanel(this);
        sizer->Add(m_string_panel1, 1, wxEXPAND);
        sizer->Add(m_string_panel2, 1, wxEXPAND);
        SetSizer(sizer);
    }

    void FlashCardPanel::reset(const wxString& string1, const wxString& string2)
    {
        m_string_panel1->show(string1);
        m_string_panel2->hide(string2);
    }

    void FlashCardPanel::reveal()
    {
        m_string_panel2->show();
    }

}
