#include "flash/flash_deck_view_config_dialog.hpp"
#include "common.hpp"

namespace flash {

    void FlashDeckViewConfigDialog::m_on_size(wxSizeEvent& event)
    {
        (void)event;
        Layout();
    }

    FlashDeckViewConfigDialog::FlashDeckViewConfigDialog(
            wxWindow *parent,
            const wxString &old_name,
            int old_page_size) :
        wxDialog(
            parent,
            wxID_ANY,
            "Configure flash deck view",
            wxDefaultPosition,
            wxDefaultSize,
            wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER)
    {
        wxBoxSizer *property_sizer;
        wxBoxSizer *main_sizer = new wxBoxSizer(wxVERTICAL);

        // 1. View name:
        property_sizer = new wxBoxSizer(wxHORIZONTAL);
        property_sizer->Add(
                new wxStaticText(this, wxID_ANY, "Name:"),
                0,
                wxEXPAND | wxALL,
                COMMON_BORDER_WIDTH);
        property_sizer->Add(
                m_name_text_ctrl = new wxTextCtrl(this, wxID_ANY, old_name),
                1,
                wxEXPAND | wxALL,
                COMMON_BORDER_WIDTH);
        main_sizer->Add(property_sizer, 0, wxEXPAND | wxALL, COMMON_BORDER_WIDTH);

        // 2. Cards per page:
        property_sizer = new wxBoxSizer(wxHORIZONTAL);
        property_sizer->Add(
                new wxStaticText(this, wxID_ANY, "Cards per page:"),
                0,
                wxEXPAND | wxALL,
                COMMON_BORDER_WIDTH);
        property_sizer->Add(
                m_page_size_spin_ctrl = new wxSpinCtrl(
                    this,
                    wxID_ANY,
                    wxString::Format(wxT("%i"), old_page_size)),
                1,
                wxEXPAND | wxALL,
                COMMON_BORDER_WIDTH);
        main_sizer->Add(property_sizer, 0, wxEXPAND | wxALL, COMMON_BORDER_WIDTH);

        wxSizer *buttons_sizer = CreateButtonSizer(wxOK | wxCANCEL);
        main_sizer->Add(buttons_sizer, 0, wxEXPAND | wxALL, COMMON_BORDER_WIDTH);

        SetSizerAndFit(main_sizer);

        Layout();
    }

    wxString FlashDeckViewConfigDialog::GetName() const
    {
        return m_name_text_ctrl->GetValue();
    }

    int FlashDeckViewConfigDialog::GetPageSize() const
    {
        return m_page_size_spin_ctrl->GetValue();
    }

    wxBEGIN_EVENT_TABLE(FlashDeckViewConfigDialog, wxDialog)
        EVT_SIZE(FlashDeckViewConfigDialog::m_on_size)
    wxEND_EVENT_TABLE()

}
