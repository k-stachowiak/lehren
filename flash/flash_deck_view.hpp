#ifndef FLASH_DECK_VIEW_HPP
#define FLASH_DECK_VIEW_HPP

#include "utility.hpp"
#include "xml_library.hpp"
#include "flash/flash_card_panel.hpp"

#include <wx/wx.h>

namespace flash {

    /// \bind A structure holding information about a word binding
    ///
    struct FlashDeckBinding {
        /// The dictionary in which the binding was found
        ///
        wxString source_dictionary;
        wxString from;              //< Binding question
        wxString to;                //< Binding answer
    };

    /// \brief Control showing a flash card learning view
    ///
    class FlashDeckView : public wxPanel,
                          public utility::XmlSerializable,
                          public utility::ChordListener {

        /// Channel for update messages
        utility::ViewUpdateChannel *m_view_update_channel;

        wxStaticText *m_dictionary_text_ctrl;     //< Source dictionary for current word
        wxToolBar *m_tool_bar;                    //< The tool bar
        FlashCardPanel *m_flash_card_panel;       //< Flash card display
        wxStaticText *m_pages_counter;            //< The label displaying page iteration state
        wxStaticText *m_words_counter;            //< The label displaying word iteration state

        int m_page_size;     //< Cards per page
        int m_current_index; //< Current index in the map of all the pairs

        /// A flat map of all the bindings for a given languages pair
        ///
        std::vector<FlashDeckBinding> m_all_pairs;

        /// \brief Iterates over all binding nodes in the non filtered out dictionaries
        ///
        template<class Callback>
        Callback m_for_each_binding_node(Callback callback)
        {
            for (const wxString &dictionary_name : dictionaries) {
                library.for_each_binding_node_in_dict(
                    dictionary_name,
                    [&callback](wxXmlNode *dictionary_node, wxXmlNode *binding_node) {
                        callback(dictionary_node, binding_node);
                    });
            }
            return callback;
        }

        /// \brief Iterates over all bound pairs in the non filtered out dictionaries
        ///
        template<class Callback>
        Callback m_for_each_bound_pair(Callback callback) const
        {
            library.for_each_bound_pair(
                source_language,
                destination_language,
                [this, &callback](wxXmlNode *dictionary_node, wxXmlNode *from, wxXmlNode *to)
                {
                    if (m_has_dictionary(dictionary_node->GetAttribute("name"))) {
                        callback(dictionary_node, from, to);
                    }
                });
            return callback;
        }

        /// \brief Checks if the deck should contain a given dictionary
        ///
        bool m_has_dictionary(const wxString &dictionary_name) const;

        /// \brief Computes the first index in the current page
        ///
        int m_first_index_in_page(int page);

        /// \brief Computes the last index in the current page
        ///
        int m_last_index_in_page(int page);

        /// \brief Computes the number of cards in the current page
        ///
        int m_current_page_size();

        /// \brief Computes the curren index in the current page
        ///
        int m_current_index_in_page();

        /// \brief Swap the source and the destination language
        ///
        /// The swap also involves the aggregation information.
        void m_on_swap_languages(wxCommandEvent &event);

        /// \brief Straightforward shuffle of the currently viewed bindings
        ///
        void m_on_shuffle_bindings(wxCommandEvent &event);

        /// \bind Launches view configuration dialog
        ///
        void m_on_configure(wxCommandEvent &event);

        /// \brief Assert the XML structural validity
        ///
        static void m_assert_dictionary(wxXmlNode *node);

        /// \brief Assert the XML structural validity
        ///
        static void m_assert_flash_deck_view(wxXmlNode *node);

        /// \brief Switches state to the previous page
        ///
        void m_on_page_previous(wxCommandEvent &event);

        /// \brief Switches state to the previous word card in the page
        ///
        void m_on_card_previous(wxCommandEvent &event);

        /// \brief Switches state to the next card in the current page
        ///
        void m_on_card_next(wxCommandEvent &event);

        /// \brief Sets state to the next page
        ///
        void m_on_page_next(wxCommandEvent &event);

        /// \brief Root command to update all the controls based on the current state
        ///
        void m_update_controls();

        /// \brief Event handling change in the library
        ///
        /// Must be called manually; will reset all the necessary member variables
        /// to maintain state correctness.
        void m_rebuild_pairs_index();

        /// \brief Common code for the controls initialization
        ///
        void m_initialize_controls();

        /// \brief Common code for initializing the non-control state
        ///
        void m_initialize_configuration();

    public:
        wxString source_path;               //< Path from which the library was loaded
        wxString name;                      //< Name of the model
        Library library;                    //< Data source
        std::vector<wxString> dictionaries; //< Dictionaries filter
        wxString source_language;           //< Source language
        wxString destination_language;      //< Destination language

        /// \brief Flag for joining all source words in a given binding
        ///
        bool aggregate_sources;

        /// \brief Flag for joining all destination words in a given binding
        ///
        bool aggregate_destinations;

        /// \brief Constructor
        ///
        /// This is an equivalent of a default constructor. After calling this the
        /// object is not in a useful state and should be additionally initialized.
        /// This constructor has been initially implemented for the purpose of
        /// deserializing this object from an XML storage.
        ///
        /// \param parent Parent window
        /// \param view_update_channel Channel for view update notifications
        FlashDeckView(
                wxWindow *parent,
                utility::ViewUpdateChannel *view_update_channel);

        /// \brief Constructor
        ///
        /// \param parent Parent window
        /// \param view_update_channel Channel for view update notifications
        /// \param source_path Path from which the view has been loaded
        /// \param library The library object
        /// \param dictionaries The dictionaries to be displayed
        /// \param source_language The question language
        /// \param destination_language The answer language
        /// \param aggregate_sources Flag for aggregating the question words
        /// \param aggregate_destinations Flag for aggregating the answer words
        FlashDeckView(
                wxWindow *parent,
                utility::ViewUpdateChannel *view_update_channel,
                const wxString &source_path,
                const Library &library,
                const std::vector<wxString> &dictionaries,
                const wxString &source_language,
                const wxString &destination_language,
                bool aggregate_sources,
                bool aggregate_destinations);

        /// \brief Implementation of the XmlSerializable superclass
        ///
        wxXmlNode *serialize() override;

        /// \brief Implementation of the XmlSerializable superclass
        ///
        void deserialize(wxXmlNode *source) override;

        /// \brief Implementation of the ChordListener superclass
        ///
        void on_chord(int mod, int key) override;

        /// \brief Tells if the deck is empty
        ///
        bool empty() const;

        /// \brief Computes the number of the pages
        ///
        int pages_count();

        /// \brief Computes the current page based on the current index
        ///
        int current_page();

        /// \brief Atomically changes the number of cards per page
        ///
        void set_page_size(int page_size);

        /// \brief Switches state to the previous page
        ///
        /// This algorithm sets the object's state to a situation in which
        /// the current word is the first word from the page before the current one.
        /// If the current page is the first one then this algorithm goes to the
        /// last page instead.
        void decrement_page();

        /// \brief Switches state to the previous word card in the page
        ///
        /// Will wrap to the last card if the beginning of the page has been reached.
        void decrement_card();

        /// \brief Switches state to the next card in the current page
        ///
        /// The details are analogous to the decrement_card
        void increment_card();

        /// \brief Sets state to the next page
        ///
        /// The details are analogous to the decrement_page
        void increment_page();

        wxDECLARE_EVENT_TABLE();
    };

}

#endif
