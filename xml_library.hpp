#ifndef XML_LIBRARY_HPP
#define XML_LIBRARY_HPP

#include <wx/xml/xml.h>

#include <functional>

class Library {

    /// \brief The library document
    ///
    wxXmlDocument m_doc;

    /// \brief Checks if node is a valid library
    ///
    static void m_assert_library(wxXmlNode *node);

    /// \brief Checks if node is a valid dictionary
    ///
    static void m_assert_dictionary(wxXmlNode *node);

    /// \brief Checks if node is a valid binding
    ///
    static void m_assert_binding(wxXmlNode *node);

public:
    Library() = default;
    Library(const Library &) = default;
    Library(Library &&) = default;
    Library &operator=(const Library &) = default;
    Library &operator=(Library &&) = default;

    Library(wxXmlDocument document);

    /// \brief Converts a word XML node into a string
    ///
    /// \param node The node to be converted
    static wxString node_to_word(wxXmlNode *node);

    /// \brief Checks if the library is ready for queries
    ///
    bool good() const;

    /// \brief Checks if the library document is empty
    ///
    bool empty() const;

    /// \brief Set internal document object
    ///
    void reset(wxXmlDocument doc);

    /// \brief Iterate over all dictionaries in the library
    ///
    /// \param callback A callback function to be called for each node
    void for_each_dictionary_node(std::function<void(wxXmlNode*)> callback) const;

    /// \brief Iterate over all the bindings in the library
    ///
    /// \param callback A callback function to be called for each
    ///        (dictionary, binding) pair.
    void for_each_binding_node(
        std::function<void(wxXmlNode *, wxXmlNode *)> callback) const;

    /// \brief Iterate over all the bindings in the dictionary of a given name
    ///
    /// \param callback A callback function to be called for each
    ///        (dictionary, binding) pair.
    void for_each_binding_node_in_dict(
        const wxString &dictionary_name,
        std::function<void(wxXmlNode *, wxXmlNode *)> callback) const;

    /// \brief Iterate over all unique language strings in the enabled dictionaries
    ///
    void for_each_language_string(
        std::function<void (const wxString &)> callback) const;

    /// \brief Iterates over all the bindings between two given languages.
    ///
    /// \param from_key The language identifier for the left argument passed to
    ///                 the callback
    /// \param to_key The language identifier for the right argument...
    /// \param callback The callback called for all the from-to binding pairs
    ///                 found in the dictionary
    /// \returns The callback object
    void for_each_bound_pair(
            const wxString& from_key,
            const wxString& to_key,
            std::function<void(wxXmlNode *, wxXmlNode *, wxXmlNode *)> callback) const;
};

#endif
