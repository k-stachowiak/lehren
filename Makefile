CXX = g++

CXXFLAGS = -g -O0 -std=c++11 -MD -MP -I. `wx-config --cxxflags` -Wall -Wextra
LDFLAGS = `wx-config --libs std,aui`

SOURCES = $(wildcard component/*.cpp flash/*.cpp workspace/*.cpp *.cpp)
OBJECTS = $(SOURCES:%.cpp=%.o)
DEPENDENCIES = $(SOURCES:%.cpp=%.d)

main: $(OBJECTS)
	$(CXX) $(CXXFLAGS) -o $@ $^ $(LDFLAGS)

.PHONY: clean

clean:
	rm -rf main *.o *.d

-include $(DEPENDENCIES)
