#ifndef UTILITY_HPP
#define UTILITY_HPP

#include <wx/wx.h>
#include <wx/xml/xml.h>

namespace flash {
    class FlashDeckView;
}

namespace utility {

    /// \brief Broadcast channel type
    ///
    template<class MessageType>
    struct BroadcastChannel {
        typedef std::function<void(MessageType)> Callback;
        std::vector<Callback> callbacks;
        void broadcast(const MessageType &message)
        {
            for (Callback callback: callbacks) {
                callback(message);
            }
        }
    };

    /// Broadcast channel specialization for a flash deck view parameter
    ///
    using ViewUpdateChannel = BroadcastChannel<flash::FlashDeckView*>;

    /// Accept notifications about a key chord being pressed by the user
    ///
    class ChordListener {
    public:
        /// Reaction to a chord
        ///
        /// \param mod The modifier, like 'C' or 'M'
        /// \param key The key pressed with the modifier
        virtual void on_chord(int mod, int key) = 0;
    };

    /// \brief Abstract base for types serializable from/to XML
    ///
    class XmlSerializable {
        /// \brief Serialize object to an XmlNode
        ///
        virtual wxXmlNode *serialize() = 0;

        /// \brief Deserialize object from an XmlNode
        ///
        virtual void deserialize(wxXmlNode *source) = 0;
    };

    /// \brief A common way of presenting a given exception in a GUI
    ///
    /// \param title Title for the message window
    /// \param ex An exception to present
    void exception_notify(const wxString& title, const std::exception& ex);

    /// \brief Joins a range of strings with a given string separator
    ///
    template<class It>
    wxString join_strings(It first, It last, const wxString &separator)
    {
        if (first == last) {
            return wxString {};
        }

        wxString result = *first++;
        while (first != last) {
            result += separator + *first++;
        }

        return result;
    }

}

#endif
