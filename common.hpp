#ifndef COMMON_HPP
#define COMMON_HPP

#include <random>

extern const int COMMON_BORDER_WIDTH;

/// Standard entropy source
///
extern std::random_device rd;

/// Mersenne twister generator
///
extern std::mt19937 g;

/// Identifiers for the wxWidgets managed events
///
enum EventIDs {
    ID_LoadFlashDeck = 1,
    ID_LoadWorkspace,
    ID_SaveWorkspace,
    ID_Browse,
    ID_ShuffleBindings,
    ID_Configure,
    ID_ReloadLibrary,
    ID_SwapLanguages,
    ID_PagePrevious,
    ID_CardPrevious,
    ID_CardNext,
    ID_PageNext,
    ID_Chord_C_P,
    ID_Chord_M_P,
    ID_Chord_C_N,
    ID_Chord_M_N,
    ID_Chord_C_SPC
};

#endif
