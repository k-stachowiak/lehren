#include "component/revealable_string_panel.hpp"

namespace cmp {

    void RevealableStringPanel::m_on_click(wxMouseEvent& event)
    {
        (void)event;
        show();
    }

    RevealableStringPanel::RevealableStringPanel(wxWindow *parent) :
            wxPanel(parent)
    {
        m_label = new wxStaticText(this, wxID_ANY, "");
        wxFont font = m_label->GetFont();
        font.SetPointSize(48);
        m_label->SetFont(font);

        wxBoxSizer *sizer = new wxBoxSizer(wxVERTICAL);
        sizer->AddStretchSpacer(1);
        sizer->Add(m_label, 0, wxALIGN_CENTER_HORIZONTAL);
        sizer->AddStretchSpacer(1);
        SetSizer(sizer);

        hide();
    }

    void RevealableStringPanel::show()
    {
        m_label->SetLabel(m_string);
        m_label->Wrap(GetSize().GetX() * 0.9);
        GetSizer()->Layout();
        SetBackgroundColour(*wxWHITE);
    }

    void RevealableStringPanel::show(const wxString& string)
    {
        m_string = string;
        show();
    }

    void RevealableStringPanel::hide()
    {
        m_label->SetLabel("");
        GetSizer()->Layout();
        SetBackgroundColour(*wxLIGHT_GREY);
    }

    void RevealableStringPanel::hide(const wxString& string)
    {
        m_string = string;
        hide();
    }

    wxBEGIN_EVENT_TABLE(RevealableStringPanel, wxPanel)
        EVT_LEFT_DOWN(RevealableStringPanel::m_on_click)
    wxEND_EVENT_TABLE()


}
