#ifndef COMPONENT_CENTERED_LABEL_PANEL_HPP
#define COMPONENT_CENTERED_LABEL_PANEL_HPP

#include <wx/wx.h>

namespace cmp {

    /// \brief A panel storing a string in the center
    ///
    class CenteredLabelPanel : public wxPanel {
    public:
        /// \brief Constructor
        ///
        /// \param parent Parent window
        /// \param string String to be displayed in the middle of the control
        CenteredLabelPanel(wxWindow *parent, const wxString &string);
    };

}

#endif
