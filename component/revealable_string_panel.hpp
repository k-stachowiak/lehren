#ifndef COMPONENT_REVEALABLE_STRING_PANEL_HPP
#define COMPONENT_REVEALABLE_STRING_PANEL_HPP

#include <wx/wx.h>

namespace cmp {

    /// \brief A panel storing a string that can be revealed upon a click
    ///
    class RevealableStringPanel : public wxPanel {

        wxString m_string;     //< The string to be presented
        wxStaticText *m_label; //< The label object to be displayed

        /// \brief Reacting to the click event
        ///
        void m_on_click(wxMouseEvent& event);

    public:
        /// \brief A constructor
        ///
        /// \param parent A pointer to the parent object needed by the base class
        RevealableStringPanel(wxWindow *parent);

        /// \brief Reveals the text associated with this control
        ///
        void show();

        /// \brief Sets a new text to this control and reveals it
        ///
        void show(const wxString& string);

        /// \brief Hides a string
        ///
        void hide();

        /// \brief Hides a string, but resets it to a new value, so that when
        ///        it is revealied it has the new value.
        void hide(const wxString& string);

        wxDECLARE_EVENT_TABLE();
    };

}

#endif
