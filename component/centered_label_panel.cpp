#include "component/centered_label_panel.hpp"

namespace cmp {

    CenteredLabelPanel::CenteredLabelPanel(wxWindow *parent, const wxString &string) :
            wxPanel(parent)
    {
        wxStaticText *static_text = new wxStaticText(this, wxID_ANY, string);
        wxBoxSizer *sizer = new wxBoxSizer(wxVERTICAL);
        sizer->AddStretchSpacer(1);
        sizer->Add(static_text, 0, wxALIGN_CENTER_HORIZONTAL);
        sizer->AddStretchSpacer(1);
        SetSizer(sizer);
    }

}
